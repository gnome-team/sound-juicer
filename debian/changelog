sound-juicer (3.40.0-2) unstable; urgency=medium

  * Stop using debian/control.in and dh-sequence-gnome
  * Bump Standards Version to 4.7.0

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 16 Jan 2025 16:14:30 -0500

sound-juicer (3.40.0-1) unstable; urgency=medium

  [ Jeremy Bícha ]
  * New upstream release
  * Drop all patches except reproducible build patch: applied in new release
  * Update standards version to 4.6.2, no changes needed

  [ Debian Janitor ]
  * Add debian/upstream/metadata
  * Update standards version to 4.6.0, no changes needed.

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 27 Jul 2023 16:12:10 +0300

sound-juicer (3.38.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Make build reproducible: Replace absolute path to source location of
    data/rhythmbox.gep embedded in binary with relative path. It would never
    resolve and is not necassary in an end-user installation (Closes: #972078)

 -- Philip Rinn <rinni@debian.org>  Wed, 26 Oct 2022 20:58:46 +0200

sound-juicer (3.38.0-2) unstable; urgency=medium

  * Drop unneeded -Wl,--as-needed and -Wno-unused-parameter
  * Cherry-pick patch to fix build with latest meson (Closes: #1005525)

 -- Jeremy Bicha <jeremy.bicha@canonical.com>  Fri, 18 Feb 2022 16:16:14 -0500

sound-juicer (3.38.0-1) unstable; urgency=medium

  * New upstream release
  * Build with meson
  * Build-Depend on gstreamer1.0-plugins-good
  * Build-Depend on debhelper-compat 13
  * Build-depend on dh-sequence-gnome instead of gnome-pkg-tools
  * Drop debian/docs: upstream build system installs these files by default
  * Cherry-pick Fix-showing-icon-in-the-about-dialog.patch
  * Add metainfo-no-network.patch:
    - Don't require network access to validate the Appstream metadata file
  * Add build-fix-doc-directory.patch:
    - Fix mistake in where docs are installed

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 05 Oct 2020 20:54:21 -0400

sound-juicer (3.24.0-3) unstable; urgency=medium

  * Update Vcs fields for migration to https://salsa.debian.org/
  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Add -Wl,-O1 -Wl,-z,defs -Wl,--as-needed to our LDFLAGS
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 27 Dec 2018 12:22:26 -0500

sound-juicer (3.24.0-2) unstable; urgency=medium

  * Update homepage
  * Update Vcs fields for conversion to git
  * Add debian/gbp.conf
  * Bump Standards-Version to 4.1.2

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 15 Dec 2017 22:38:08 -0500

sound-juicer (3.24.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in:
    - Drop obsolete Build-Depends on gnome-common (Closes: #829998)
    - Depend on default-dbus-session-bus | dbus-session-bus
  * Bump Standards-Version to 4.1.0

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 31 Aug 2017 13:43:06 -0400

sound-juicer (3.22.1-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Mon, 10 Oct 2016 16:53:14 +0200

sound-juicer (3.22.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump dh compat to 10 (automatic dh-autoreconf).

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 19 Sep 2016 21:07:34 +0200

sound-juicer (3.21.92-1) unstable; urgency=medium

  * New upstream beta release.
  * Update build-dependencies according to configure.ac changes:
    - bump libglib2.0-dev to >= 2.49.5

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 13 Sep 2016 15:22:42 +0200

sound-juicer (3.21.90-1) unstable; urgency=medium

  * New upstream beta release.
  * Update Build-Depends as per configure.ac:
    - Drop intltool.
    - Bump libdiscid-dev to (>= 0.4.0).
    - Bump libgtk-3-dev to (>= 3.21.3).
  * Drop obsolete --disable-scrollkeeper configure flag.
  * Convert from cdbs to dh.

 -- Michael Biebl <biebl@debian.org>  Sat, 10 Sep 2016 15:49:21 +0200

sound-juicer (3.20.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Build-Depends on libgtk-3-dev to (>= 3.19.6) as per configure.ac.
  * Bump Standards-Version to 3.9.8.

 -- Michael Biebl <biebl@debian.org>  Wed, 20 Apr 2016 02:19:59 +0200

sound-juicer (3.18.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump debhelper compatibility level to 9.
  * Bump Standards-Version to 3.9.6.
  * Drop debian/sound-juicer.manpages, not necessary since the man page is
    already installed by the upstream build system.

 -- Michael Biebl <biebl@debian.org>  Fri, 20 Nov 2015 02:31:40 +0100

sound-juicer (3.18.0-1) unstable; urgency=medium

  * New upstream release.
  * Remove Debian menu entry.

 -- Michael Biebl <biebl@debian.org>  Mon, 21 Sep 2015 15:46:39 +0200

sound-juicer (3.16.1-1) unstable; urgency=medium

  * Drop eject recommends - obsolete since circa 2003
  * New upstream release.
  * Add build-dependency on appstream-util for new appdata file generation.

 -- Andreas Henriksson <andreas@fatal.se>  Sun, 17 May 2015 11:58:17 +0200

sound-juicer (3.14.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump build-dependencies according to configure.ac changes:
    + libglib2.0-dev (>= 2.38),
    + libgtk-3-dev (>= 3.4.0),

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 22 Sep 2014 22:03:58 +0200

sound-juicer (3.12.0-2) unstable; urgency=medium

  [ Andreas Henriksson ]
  * Run wrap-and-sort
  * Build-depend on libdiscid-dev instead of libdiscid0-dev (Closes: #753638)

  [ Laurent Bigonville ]
  * debian/control.in: Drop build-dependency against
    libgnome-media-profiles-dev, this seems to be obsolete for quite some time

 -- Laurent Bigonville <bigon@debian.org>  Wed, 10 Sep 2014 15:19:46 +0200

sound-juicer (3.12.0-1) unstable; urgency=medium

  * New upstream release.
  * Update dependencies according to configure.ac:
    - build-depend on gsettings-desktop-schemas-dev
    - make sound-juicer depend on gsettings-desktop-schemas

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 14 Jun 2014 13:48:46 +0200

sound-juicer (3.11.90-1) experimental; urgency=medium

  * New upstream git snapshot (3.5.0+git20131005.2bc27d51)
  * Update (build-)dependencies for GStreamer 1.0 port
  * Bump build-dependencies:
    - Gtk+ to >= 3.2.0
    - Glib to >= 2.32
  * Add build-dependency and dependency on iso-codes
  * Drop debian/patches/0001-Use-musicbrainz5-drop-earlier-versions.patch
    - now in upstream version
  * Change appending CFLAGS and LDFLAGS to use DEB_*_MAINT_APPEND equivalent
  * Use DEB_DH_AUTORECONF_ARGS += ./autogen.sh
  * New upstream release (3.11.90)
  * Update build-dependencies further according to configure.ac changes:
    - drop libgconf2-dev (GConf obsoleted by GSettings)
    - replace scrollkeeper and gnome-doc-utils with yelp-tools
  * Bump Standards-Version to 3.9.5

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 16 May 2014 18:33:30 +0200

sound-juicer (3.4.0-3) unstable; urgency=low

  [ Jon Dowland ]
  * Build against libmusicbrainz5-dev. This permits use of the
    newer MB API, which means multi-disc releases are handled
    properly again. Closes: #644512, #581417, #644993, #677232.

  [ Michael Biebl ]
  * Use dh-autoreconf to update the build system.

 -- Michael Biebl <biebl@debian.org>  Tue, 19 Jun 2012 22:50:06 +0200

sound-juicer (3.4.0-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: Track .xz tarballs.
  * debian/rules: Stop calling autogen.sh now that we have a proper release
    tarball.
  * Drop Build-Depends on libdbus-1-dev and libdbus-glib-1-dev, sound-juicer
    has been ported to GDBus.
  * Drop obsolete Build-Conflicts on libcdio-dev.
  * Bump Standards-Version to 3.9.3.

 -- Michael Biebl <biebl@debian.org>  Sat, 31 Mar 2012 06:48:27 +0200

sound-juicer (2.32.1+git20120102.e678f0a-1) unstable; urgency=low

  [ Josselin Mouette ]
  * Update repository URL.

  [ Michael Biebl ]
  * New upstream Git snapshot e678f0a13846d33eb580118587908f9eaf8128be.
  * Run autogen.sh to generate the build system.
  * Add Build-Depends on gnome-common for gnome-autogen.sh.

 -- Michael Biebl <biebl@debian.org>  Mon, 02 Jan 2012 17:10:23 +0100

sound-juicer (2.32.1+git20111010.2d5deeb-1) unstable; urgency=low

  * New upstream Git snapshot 2d5deeb9bf9bf2eb9389b61de81ef2017cf8e0d0.
  * debian/control.in:
    - Re-add Build-Depends on libneon27-gnutls-dev, see #628696.
    - Bump Standards-Version to 3.9.2. No further changes.
    - Set pkg-gnome-maintainers@lists.alioth.debian.org as Maintainer.
  * Upload to unstable.

 -- Michael Biebl <biebl@debian.org>  Fri, 14 Oct 2011 05:02:22 +0200

sound-juicer (2.32.1+20110330-1) experimental; urgency=low

  * New upstream git snapshot, with gtk3 support.
  * Update build-dependencies accordingly.
  * Fix FSF address in copyright.

 -- Josselin Mouette <joss@debian.org>  Wed, 30 Mar 2011 20:02:48 +0530

sound-juicer (2.32.0-1) unstable; urgency=low

  * New upstream release.
    - Fixes build failures due to conflicting types. Closes: #614452
    - Fixes build failures with binutils-gold / GCC 4.5. Closes: #592373
  * Switch to source format 3.0 (quilt).
    - Add debian/source/format.
  * Update build dependencies.
    - Drop libglade2-dev.
    - Drop libneon27-gnutls-dev.
    - Bump libgtk2.0-dev to (>= 2.20).
    - Bump libbrasero-media-dev to (>= 2.26).
    - Bump debhelper to (>= 8).
  * Bump Standards-Version to 3.9.1. No further changes.
  * Bump debhelper compatibility level to 8.

 -- Michael Biebl <biebl@debian.org>  Wed, 16 Mar 2011 18:12:57 +0100

sound-juicer (2.28.2-3) unstable; urgency=low

  * Add libcdio-dev to build conflicts so the libcdio backend won't
    be accidentally enabled.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Tue, 05 Oct 2010 14:18:39 +0200

sound-juicer (2.28.2-2) unstable; urgency=low

  [ Josselin Mouette ]
  * Drop libcdio build-dependency since the backend is broken and causes
    crashes. Closes: #590878.

  [ Emilio Pozuelo Monfort ]
  * Upload to unstable.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Fri, 01 Oct 2010 13:48:42 +0200

sound-juicer (2.28.2-1) unstable; urgency=low

  * New upstream bugfix release.
  * debian/control.in:
    + Use libmusicbrainz 3 instead of the deprecated 2.1 release series.

 -- Sebastian Dröge <slomo@debian.org>  Fri, 09 Apr 2010 07:47:21 +0200

sound-juicer (2.28.1-2) unstable; urgency=low

  * debian/control.in:
    - Stop depending on hal, it's no longer needed. Closes: #558455.
  * Standards-Version is 3.8.4, no changes needed.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Sat, 06 Feb 2010 11:37:30 +0100

sound-juicer (2.28.1-1) unstable; urgency=low

  * New upstream release.
    - Build depend on libdbus-1-dev.
  * debian/control.in:
    - Standards-Version is 3.8.3, no changes needed.
    - Point Vcs-* to the unstable branches.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Fri, 27 Nov 2009 11:32:27 +0100

sound-juicer (2.28.0-1) unstable; urgency=low

  [ Loïc Minier ]
  * Suggest brasero.

  [ Andreas Henriksson ]
  * New upstream release.
  * bump build-dependency on libglib2.0-dev to >= 2.18.

 -- Emilio Pozuelo Monfort <pochu@debian.org>  Wed, 23 Sep 2009 18:17:08 +0200

sound-juicer (2.26.1-1) unstable; urgency=low

  [ Emilio Pozuelo Monfort ]
  * Suggest gstreamer0.10-plugins-ugly rather than -bad, the twolame
    plugin has been moved to -ugly. Update the description too.
    Thanks to Fabian Greffrath.

  [ Josselin Mouette ]
  * Stop mentioning GNOME 2.

  [ Emilio Pozuelo Monfort ]
  * New upstream version.
    + Actually sets disc number. Closes: #525468.
  * Update build-dependencies.
  * debian/patches/01_manpage_error.patch: removed, fixed upstream.
  * debian/rules: remove simple-patchsys.mk
  * debian/copyright:
    - Point to GPL-2 rather than GPL.
    - Updated.
  * Section is sound.
  * Standards-Version is 3.8.2, no changes needed.

  [ Josselin Mouette ]
  * Add missing build-dependency on libcdio-dev.

 -- Josselin Mouette <joss@debian.org>  Wed, 08 Jul 2009 00:15:24 +0200

sound-juicer (2.24.0-2) unstable; urgency=low

  * Remove suggests on -ugly, since there are no profiles using
    it by default.
  * Add suggests on -lame and -really-bad for MP3 and AAC support.
    Explain why you might need them in the description. Closes: #509787.
  * Upload to unstable.

 -- Josselin Mouette <joss@debian.org>  Thu, 05 Mar 2009 16:33:03 +0100

sound-juicer (2.24.0-1) experimental; urgency=low

  [ Emilio Pozuelo Monfort ]
  * New upstream release.
    - Fixes a crash when accessing the preferences. Closes: #490346.
    - Adds a year filename tag. Closes: #471294.
    - Shows the tracks in the main window when launched by nautilus.
      Closes: #479570.
    - Update build dependencies.
    - Remove gstreamer0.10-gnomevfs dependency, it now uses giosink.
    - debian/patches/01_musicbrainz_message.patch: removed, fixed upstream.
  * debian/watch: Remove 'debian uupdate'.
  * debian/control.in:
    - Wrap Build-Depends and Depends.
    - Update Standards-Version to 3.8.0, no changes needed.
    - Added Homepage and Vcs-* fields.
  * debian/menu: Fix section.
  * debian/copyright:
    - Use correct download url in debian/copyright.
    - Add copyright holders.
  * debian/sound-juicer.1,
    debian/sound-juicer:
    - Remove the manpage, as it's shipped upstream, and install it from
      the tarball  * debian/sound-juicer.1,
    debian/sound-juicer:
    - Remove the manpage, as it's shipped upstream, and install it from
      the tarball.

  [ Josselin Mouette ]
  * Require gstreamer0.10-plugins-base 0.10.20 for the availability of
    giosink.
  * Pass -01 -z defs --as-needed to the linker.
  * 01_manpage_error.patch: new patch. Fix a small error in the manual
    page.

 -- Josselin Mouette <joss@debian.org>  Wed, 10 Dec 2008 10:18:35 +0100

sound-juicer (2.22.0-2) unstable; urgency=low

  * 01_musicbrainz_message.patch: patch from Pekka Vuorela to fix
    display issues with the musicbrainz notification. Upstream commit
    r2179. Closes: #497261.
  * Remove versioned build-dependency on scrollkeeper.

 -- Josselin Mouette <joss@debian.org>  Thu, 18 Sep 2008 17:23:18 +0200

sound-juicer (2.22.0-1) unstable; urgency=low

  [ Josselin Mouette ]
  * Build-depend on libtag1-dev.

  [ Sebastian Dröge ]
  * New upstream stable release:
    + debian/control.in:
      - Update build dependencies.
      - Update Standards-Version to 3.7.3, no additional changes needed.
      - Suggest gst-plugins-ugly and gst-plugins-bad.

 -- Sebastian Dröge <slomo@debian.org>  Fri, 14 Mar 2008 13:09:50 +0100

sound-juicer (2.20.1-1) unstable; urgency=low

  * New upstream bugfix release:
    + debian/patches/20_svn_edit_profiles_crash.patch:
      - Dropped, merged upstream.

 -- Sebastian Dröge <slomo@debian.org>  Sun, 14 Oct 2007 18:30:55 +0200

sound-juicer (2.20.0-2) unstable; urgency=low

  * debian/patches/20_svn_edit_profiles_crash.patch:
    + Patch from upstream SVN to fix crashes when editing profiles
      or doing something else with profiles (Closes: #443146).

 -- Sebastian Dröge <slomo@debian.org>  Thu, 20 Sep 2007 21:28:23 +0200

sound-juicer (2.20.0-1) unstable; urgency=low

  [ Sven Arvidsson ]
  * Add depend on hal (Closes: #422322)

  [ Sebastian Dröge ]
  * New upstream release:
    + debian/control.in:
      - Update musicbrainz and gstreamer requirements.

 -- Sebastian Dröge <slomo@debian.org>  Mon, 17 Sep 2007 12:06:04 +0200

sound-juicer (2.16.4-1) unstable; urgency=low

  [ Sven Arvidsson ]
  * Add icon to the Debian menu entry (Closes: #252849)

  [ Sebastian Dröge ]
  * New upstream release.
  * Upload to unstable, drop check-dist include.
  * Update build dependencies from configure.in.
  * Update debhelper compat level to 5.
  * Remove the (non-existing) doap.rdf file from debian/docs.

 -- Sebastian Dröge <slomo@debian.org>  Wed, 25 Apr 2007 16:03:47 +0200

sound-juicer (2.16.3-1) experimental; urgency=low

  * Add a get-orig-source target to retrieve the upstream tarball.
  * Include the new check-dist Makefile to prevent accidental uploads to
    unstable; bump build-dep on gnome-pkg-tools to >= 0.10.
  * Merge 2.14.6-1.
  * New upstream release.
  * Add libgtk2.0-dev >= 2.10 build-dep to make the package autobuildable.

 -- Loic Minier <lool@dooz.org>  Mon, 12 Mar 2007 11:33:46 +0100

sound-juicer (2.16.2-1) experimental; urgency=low

  * New upstream release.
  * Remove duplicate build-dependency on gnome-vfs.

 -- Josselin Mouette <joss@debian.org>  Sat,  9 Dec 2006 00:11:50 +0100

sound-juicer (2.16.1-1) experimental; urgency=low

  * Add CDBS' utils.
  * Don't override DEB_CONFIGURE_EXTRA_FLAGS.
  * New upstream release, "Now Every Fool In Town".
  * Install doap.rdf.
  * Bump up build-deps to libgnomeui-dev >= 2.16.0-2 and libgnomevfs2-dev >=
    2.10.0 because of the new libnautilus-burn-dev >= 2.15.

 -- Loic Minier <lool@dooz.org>  Thu,  5 Oct 2006 18:43:45 +0200

sound-juicer (2.16.0-1) experimental; urgency=low

  [ Marco Cabizza ]
  * New upstream release, target experimental:
    - Bump libgtk2.0-dev build-dep to 2.8 and libnautilus-burn-dev to 2.15.3

  [ Loic Minier ]
  * Bump libgnomeui-dev build-dep to >= 2.13.0.

 -- Loic Minier <lool@dooz.org>  Thu, 28 Sep 2006 15:57:09 +0200

sound-juicer (2.14.6-1) unstable; urgency=low

  [ Ross Burton ]
  * New upstream release
    - Fixes writing to remote locations (Closes: #410590)
    - Stop playing properly when the disc is ejected (Closes: #396247)

  [ Sven Arvidsson ]
  * Add a man page (Closes: #358205)

  [ Loic Minier ]
  * Add a get-orig-source target to retrieve the upstream tarball.

 -- Ross Burton <ross@debian.org>  Fri,  9 Mar 2007 20:18:29 +0000

sound-juicer (2.14.5-1) unstable; urgency=low

  * New upstream release.
  * Track stable versions in watch file.

 -- Loic Minier <lool@dooz.org>  Mon,  7 Aug 2006 22:02:10 +0200

sound-juicer (2.14.4-2) unstable; urgency=medium

  * Add a gstreamer0.10-gnomevfs Depends to sound-juicer, thanks Alec
    Berryman. (Closes: #373695)

 -- Loic Minier <lool@dooz.org>  Thu, 15 Jun 2006 09:28:09 +0200

sound-juicer (2.14.4-1) unstable; urgency=low

  [ Marco Cabizza ]
  * New upstream version:
    - fixed the no disk space issue (Closes: #349861)
  * debian/control, debian/control.in:
    - Dropping gstreamer 0.8, depending on 0.10
    - Standards-version is 3.7.2
  * debian/patches/01_fix_keybinding.diff:
    - Dropped, merged upstream

  [ Josselin Mouette ]
  * Even newer upstream version.

 -- Josselin Mouette <joss@debian.org>  Fri,  9 Jun 2006 01:08:40 +0200

sound-juicer (2.12.3-4) unstable; urgency=low

  * debian/control.in:
    - doesn't Depends on gnome-media

  [ Loic Minier ]
  Loic Minier <lool@dooz.org>:
  * Bump up gnome-media build-dep to >= 2.11.91. (Closes: #353001)
    [debian/control, debian/control.in]

  Gustavo Noronha Silva <kov@debian.org>
  * debian/patches/01_fix_keybinding.diff:
  - patch from upstream CVS to fix keybinding that was masking
    the paste keybinding

  [ Josselin Mouette ]
  * Build-depend on libgnome-media-dev instead of gnome-media
    (closes: #357639).

 -- Sebastien Bacher <seb128@debian.org>  Thu,  6 Apr 2006 14:42:49 +0200

sound-juicer (2.12.3-3) unstable; urgency=low

  * Upload to unstable.
  * Standards-version is 3.6.2.

 -- Josselin Mouette <joss@debian.org>  Sun,  8 Jan 2006 12:47:17 +0100

sound-juicer (2.12.3-2) experimental; urgency=low

  * Rebuild to actually get the new musicbrainz packages.

 -- Ross Burton <ross@debian.org>  Thu,  1 Dec 2005 10:42:08 +0000

sound-juicer (2.12.3-1) experimental; urgency=low

  * New upstream release
  * Bump build-dep on Musicbrainz to get transitioned package
  * Depend on gnome-media 2.12, grrr
  * Update copyright file with new FSF address

 -- Ross Burton <ross@debian.org>  Thu,  1 Dec 2005 09:39:30 +0000

sound-juicer (2.12.2-1) experimental; urgency=low

  * New upstream release
  * Add build-depend on libgstreamer-plugins0.8-dev and gnome-doc-utils
  * Add depend on gstreamer0.8-gnomevfs

 -- Sjoerd Simons <sjoerd@debian.org>  Thu, 22 Sep 2005 00:01:32 +0200

sound-juicer (2.10.1-3) unstable; urgency=low

  * Rebuild for C++ transition and increase build-dep on musicbrainz
    (closes: #321541)

 -- Ross Burton <ross@debian.org>  Sat,  6 Aug 2005 12:12:25 +0100

sound-juicer (2.10.1-2) unstable; urgency=low

  * The "Ross Burton escapes from the angry mob" release.
  * Upload to unstable, on behalf of Ross.

 -- Jordi Mallach <jordi@debian.org>  Mon, 13 Jun 2005 16:29:23 +0200

sound-juicer (2.10.1-1) experimental; urgency=low

  * New upstream release
  * Add build-deps on gnomevfs 2.10 and nautilus-burn 2.10.

 -- Ross Burton <ross@debian.org>  Mon,  9 May 2005 16:01:30 +0100

sound-juicer (0.6.1-2) unstable; urgency=low

  * Add build-dep on libgstreamer0.8-gconf (closes: #303884)

 -- Ross Burton <ross@debian.org>  Sat,  9 Apr 2005 14:00:22 +0100

sound-juicer (0.6.1-1) unstable; urgency=low

  * New upstream release
    - Add build-deps on GTK+ 2.6 and gnome-media
    - Fixes invalid cast on AMD64 (closes: #288535)
  * Add debian/watch

 -- Ross Burton <ross@debian.org>  Fri,  8 Apr 2005 14:36:56 +0100

sound-juicer (0.5.15-1) unstable; urgency=low

  * New upstream release
    - UTF8 characters handled better (closes: #247373)
    - Drive logic fixed (closes: #259985)

 -- Ross Burton <ross@debian.org>  Thu,  2 Dec 2004 19:57:26 +0000

sound-juicer (0.5.14-1) unstable; urgency=low

  * New upstream release
    - I'm the Debian and upstream maintainer, no need to remind me about
    new releases. (closes: #275961)
    - Alt-E is not Extract shortcut any more (closes: #256341)
    - uses non-blocking open (closes: #218266)
    - doesn't set the Comment field (closes: #252894)

 -- Ross Burton <ross@debian.org>  Mon, 11 Oct 2004 15:31:52 +0100

sound-juicer (0.5.13-1) unstable; urgency=low

  * New upstream release.
  * Build-depend on GTK+ 2.4.

 -- Ross Burton <ross@debian.org>  Tue, 28 Sep 2004 14:01:46 +0100

sound-juicer (0.5.12-2) unstable; urgency=low

  * Remove the postinst, dh_gconf handles this.

 -- Ross Burton <ross@debian.org>  Wed, 11 Aug 2004 12:29:21 +0100

sound-juicer (0.5.12-1) unstable; urgency=low

  * New upstream release.

 -- Ross Burton <ross@debian.org>  Mon,  7 Jun 2004 18:01:55 +0100

sound-juicer (0.5.11-2) unstable; urgency=low

  * GNOME Team Upload.
  * Upload in unstable (Closes: #251573).
  * debian/control.in:
    + updated Standards-Version to 3.6.1.0.

 -- Sebastien Bacher <seb128@debian.org>  Sun, 30 May 2004 01:54:05 +0200

sound-juicer (0.5.11-1) experimental; urgency=low

  * New upstream release.
  * debian/control.in: Bump deps to GStreamer 0.8
  * debian/menu: Fix lintian quote warnings

 -- Ross Burton <ross@debian.org>  Tue, 13 Apr 2004 11:35:29 +0100

sound-juicer (0.5.10.1-3) unstable; urgency=low

  * Bump Build-Depends on debhelper to get dh_gconf.

 -- Ross Burton <ross@debian.org>  Thu, 19 Feb 2004 10:11:15 +0000

sound-juicer (0.5.10.1-2) unstable; urgency=low

  * Add Build-Depend on intltool (closes: #231369, #231379)

 -- Ross Burton <ross@debian.org>  Fri,  6 Feb 2004 10:40:28 +0000

sound-juicer (0.5.10.1-1) unstable; urgency=low

  * New upstream release, fixing intltool for everyone else.

 -- Ross Burton <ross@debian.org>  Thu,  5 Feb 2004 16:23:07 +0000

sound-juicer (0.5.10-1) unstable; urgency=low

  * New upstream release (closes: #230234)
  * Move to GNOME Packaging Team

 -- Ross Burton <ross@debian.org>  Thu,  5 Feb 2004 16:23:07 +0000

sound-juicer (0.5.9-1) unstable; urgency=low

  * New upstream release

 -- Ross Burton <ross@debian.org>  Wed,  7 Jan 2004 12:18:44 +0000

sound-juicer (0.5.8-1) unstable; urgency=low

  * New upstream release

 -- Ross Burton <ross@debian.org>  Wed, 26 Nov 2003 16:54:28 +0000

sound-juicer (0.5.7-1) unstable; urgency=low

  * New upstream release
    - Encoder Missing dialog works (closes: #218875)
    - Fixed GStreamer requirements (closes: #217975)

 -- Ross Burton <ross@debian.org>  Tue, 11 Nov 2003 14:20:57 +0000

sound-juicer (0.5.6-1) unstable; urgency=low

  * New upstream release

 -- Ross Burton <ross@debian.org>  Mon, 27 Oct 2003 11:23:16 +0000

sound-juicer (0.5.5-1) unstable; urgency=low

  * New upstream release

 -- Ross Burton <ross@debian.org>  Wed,  1 Oct 2003 15:24:40 +0100

sound-juicer (0.5.4-2) unstable; urgency=low

  * Build-depend on scrollkeeper (closes: #212502, #212529)

 -- Ross Burton <ross@debian.org>  Wed, 24 Sep 2003 10:16:52 +0100

sound-juicer (0.5.4-1) unstable; urgency=low

  * New upstream release

 -- Ross Burton <ross@debian.org>  Tue, 16 Sep 2003 20:39:45 +0100

sound-juicer (0.5.3-1) unstable; urgency=low

  * New upstream release
  * Push Standards to 3.6.0.
  * Turn off nostrip.

 -- Ross Burton <ross@debian.org>  Tue, 16 Sep 2003 14:57:51 +0100

sound-juicer (0.5.2-1) unstable; urgency=low

  * New upstream release

 -- Ross Burton <ross@debian.org>  Thu, 21 Aug 2003 13:55:30 +0100

sound-juicer (0.5.1-2) unstable; urgency=low

  * debian/rules: set nostrip so that we get good back traces.

 -- Ross Burton <ross@debian.org>  Thu, 14 Aug 2003 18:35:59 +0100

sound-juicer (0.5.1-1) unstable; urgency=low

  * New upstream release.

 -- Ross Burton <ross@debian.org>  Mon, 11 Aug 2003 14:01:30 +0100

sound-juicer (0.4.1-1) unstable; urgency=low

  * New upstream release.

 -- Ross Burton <ross@debian.org>  Tue, 24 Jun 2003 13:55:13 +0100

sound-juicer (0.3-2) unstable; urgency=low

  * Added debian/menu file to stop Thom May moaning.

 -- Ross Burton <ross@debian.org>  Tue, 20 May 2003 16:51:40 +0100

sound-juicer (0.3-1) unstable; urgency=low

  * New upstream release
  * Re-arranged the GStreamer deps.
  * Change to CDBS

 -- Ross Burton <ross@debian.org>  Tue, 20 May 2003 11:11:11 +0100

sound-juicer (0.2.1-1) unstable; urgency=low

  * New upstream release

 -- Ross Burton <ross@debian.org>  Tue, 29 Apr 2003 15:30:59 +0100

sound-juicer (0.2-1) unstable; urgency=low

  * New upstream release

 -- Ross Burton <ross@debian.org>  Tue, 29 Apr 2003 14:17:05 +0100

sound-juicer (0.1-1) unstable; urgency=low

  * Initial Release.

 -- Ross Burton <ross@burtonini.com>  Fri, 11 Apr 2003 16:07:19 +0100
